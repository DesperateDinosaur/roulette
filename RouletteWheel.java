import java.util.Random;

public class RouletteWheel{
    //Field initialization
    private Random rand;
    private int spinNum;

    //Constructor
    public RouletteWheel(){
        this.rand = new Random();
    }

    //Method to update the spinNum field
    public void spin(){
        this.spinNum = this.rand.nextInt(37);
    }

    //Method to return value representing last spin
    public int getValue(){
        return(this.spinNum);
    }
}