import java.util.Scanner;

public class Roulette{
    public static void main(String[] args){
        //RouletteWheel object
        RouletteWheel wheel = new RouletteWheel();

        //Initializing scanner, game status, and variables
        Scanner ask = new Scanner(System.in);

        Boolean isActive = true;

        String response;
        int number;
        int bet;
        int winnings = 0;

        //Game loop
        while(isActive == true){

            //Ask user to bet
            System.out.println("Would you like to make a bet? (y/n)");
            response = ask.nextLine();
            
            //Yes response
            if(response.equals("y")){
                System.out.println("Which number would you like to bet on? (0-36)");
                number = ask.nextInt();

                System.out.println("How much would you like to bet?");
                bet = ask.nextInt();
                
                //Spin wheel and check results
                wheel.spin();
                int result = wheel.getValue();

                //Win
                if(number == result){
                    System.out.println(". Congratulations! You landed on " + result);

                    bet = bet*35;
                    System.out.println("You win $" + bet + "!");

                    winnings = winnings + bet;
                }

                //Lose
                else{
                    System.out.println("Sorry, you landed on " + result);
                }

                System.out.println("Your current earnings are $" + winnings + "!");
            }

            //No response
            else if(response.equals("n")){
                isActive = false;
                System.out.println("Thank's for playing!");
            }
        }

        //Closing scanner
        ask.close();
    }
}